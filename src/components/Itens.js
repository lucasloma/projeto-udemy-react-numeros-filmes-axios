import React from 'react';
import { StyleSheet, Text, View,Image,TouchableOpacity,Alert } from 'react-native';


export default class Itens extends React.Component {
    constructor(props){
        super(props);

    }

    render() {
        const {itens} = this.props;
        return (
            <TouchableOpacity
                onPress={()=>(Alert.alert('Sinopse',itens.sinopse))}
                style={styles.container}>
                <View style={styles.card}>
                    <Image
                        source = {{uri:itens.poster}}
                        aspectRatio = {1}
                        resizeMode = "cover"
                    />
                    <View style={styles.cardTitleWrapper}>
                        <Text style={styles.cardTitle}>
                            {itens.nome}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({
    container:{
         flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        paddingTop:10
    },
    card:{
        flex:1,
        borderWidth:1,
    },
    cardTitleWrapper:{
        backgroundColor:'#1E90FF',
        height: 40,
        position:'absolute',
        bottom: 0,
        opacity: .8,
        width: '100%',
        paddingTop: 5,
        paddingBottom:5,
        paddingLeft:3,
        paddingRight:3,
        alignItems:'center'
    },
    cardTitle:{
        color:'white',
        fontWeight:'bold',
        fontSize: 13,
    },

});