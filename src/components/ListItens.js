import React from 'react';
import { StyleSheet, Text, View,ScrollView} from 'react-native';
import Itens from './Itens';
import axios from "axios/index";

export default class ListItens extends React.Component {
    constructor(props){
        super(props);
        //Construido aplicação
        this.state = {listaFilmes:[ ]}
    }

    componentWillMount(){
        //Antes da aplicação
        //  requisição Html
        //  axios.get('http://faus.com.br/recursos/c/dmairr/api/itens.html').
        //  then(respost => {console.log(respost)}).
        //  catch(error => {console.log(error)});
        const URL = 'https://new-movies-brazil.herokuapp.com/api/v1/filmes';
        axios.get(URL)
            .then(response => {
                this.setState({listaFilmes: response.data.filmes});
            })
            .catch(error => {
                console.log(error.message)
            });
    }

    componentDidMount(){
        //Depois da Aplicação

    }

    render() {
        //reiderizado
        return(
            <View>
                <View style = {style.container}>
                    <Text style ={style.title}>Filmes</Text>
                </View>
                <ScrollView>
                    {this.state.listaFilmes.map(iten => (
                        <Itens key={iten.nome} itens={iten}/>
                    ))}
                </ScrollView>
            </View>
        );
    }
}

const style = StyleSheet.create({

    container: {
        marginTop: 25,
        backgroundColor: '#1E90FF',
        alignItems: 'center',
        justifyContent: 'center',
    },

    title: {
        fontSize: 35,
        color: '#fff'
    }

});

